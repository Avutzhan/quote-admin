const express = require('express')
const bodyParser = require('body-parser')
const app = express();
const MongoClient = require('mongodb').MongoClient

const connectionString = "mongodb+srv://avutzhan:hotafo90@cluster0.pdx2a.mongodb.net/myFirstDatabase?retryWrites=true&w=majority";

//set view engine to use EJS
app.set('view engine', 'ejs')

//we need db variable so we need to put our handlers inside Mongo Promise
//Middleware using before crud handlers

//urlencoded extract data from form element and add them to the body property in the request object
app.use(bodyParser.urlencoded({ extended: true }))
//static middleware makes public accessible
app.use(express.static('public'))
//json from body parser teach server to work with json data
app.use(bodyParser.json())

//Configure Mongo
MongoClient.connect(connectionString, { useUnifiedTopology: true })
    .then(client => {
        console.log('Connected to Database')
        //change db name
        const db = client.db('start-wars-quotes')
        const quotesCollection = db.collection('quotes')

        //Request Handlers
        //REST/READ main page route
        app.get('/', (req, res) => {
            db.collection('quotes').find().toArray()
                .then(results => {
                    res.render('index.ejs', { quotes: results })
                })
                .catch(error => console.error(error))

        })

        //REST/CREATE quote
        app.post('/quotes', (req, res) => {
            //insertOne to add item to MongoDB collection
            quotesCollection.insertOne(req.body)
                .then(result => {
                    console.log(result)
                    //browser expect something back from the server but we dont need to send back something we just
                    //redirect to main page
                    res.redirect('/')
                })
                .catch(error => console.error(error))
        })

        //REST/PUT change
        app.put('/quotes', (req, res) => {
            quotesCollection.findOneAndUpdate(
                    { name: 'Yoda' },
                    {
                        $set: {
                            name: req.body.name,
                            quote: req.body.quote
                        }
                    },
                    {
                        upsert: true
                    }
                )
                .then(result => {
                    res.json('Success')
                })
                .catch(error => console.error(error))
        })

        //REST/DELETE
        app.delete('/quotes', (req, res) => {
            quotesCollection.deleteOne(
                    { name: req.body.name }
                )
                .then(result => {
                    if (result.deletedCount === 0) {
                        return res.json('No quote to delete')
                    }
                    res.json(`Deleted Darth Vadar's quote`)
                })
                .catch(error => console.error(error))
        })

        app.listen(3000, function () {
            console.log('listening on 3000')
        });

    })
    .catch(error => console.error(error))


